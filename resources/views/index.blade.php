@extends('layouts.app')
@section('title', 'Home')
@section('content')

    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"
                aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
                aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
                aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{ asset('assets/image/homepage/bg1.jpg') }}" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5 style="color:black">First slide label</h5>
                    <p style="color:black">Some representative placeholder content for the first slide.</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="{{ asset('assets/image/homepage/bg1.jpg') }}" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5 style="color:black">First slide label</h5>
                    <p style="color:black">Some representative placeholder content for the first slide.</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="{{ asset('assets/image/homepage/bg1.jpg') }}" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5 style="color:black">First slide label</h5>
                    <p style="color:black">Some representative placeholder content for the first slide.</p>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
            data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
            data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <div class="container-fluid">
        <div class="row justify-content-md-center p-5">
            <h1 class="text-center mb-5"><span style="color: #E55401">OUR </span><span
                    style="color: #398DA5">SERVICES</span></h1>
            <div class="col-md-3 text-center mb-3 justify-content-center" data-aos="fade-up">
                <div class="card">
                    <div class="box-img "
                        style="background-image: url( https://www.flazio.com/public/componenti/48321/f1/creare-un-sito.png );">
                        <div class="hitam-trans d-flex justify-content-center align-items-center"
                            style="background-color: rgb(0, 0, 0);">
                            <h2 class="" style=" color: white;"> <i class="fa fa-globe" aria-hidden="true"></i>
                            </h2>
                        </div>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Web Development</h5>
                        <p class="card-text">Lorem Ipsum doset amet kontomet anumet ajuajkan </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-center mb-3" data-aos="fade-up" data-aos-delay="200">
                <div class="card">
                    <div class="box-img"
                        style="background-image: url( https://blog.trello.com/hubfs/2017-09-06_Summit2017_TrelloForDesktop_r02.png );">
                        <div class="hitam-trans d-flex justify-content-center align-items-center"
                            style="background-color: rgb(0, 0, 0);">
                            <h2 class="teks-overlay" style="color: white;"> <i class="fa fa-globe" aria-hidden="true"></i>
                            </h2>
                        </div>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Desktop App Development</h5>
                        <p class="card-text">Lorem Ipsum doset amet kontomet anumet ajuajkan </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-center mb-3" data-aos="fade-up" data-aos-delay="300">
                <div class="card">
                    <div class="box-img"
                        style="background-image: url( https://technojogja.com/wp-content/uploads/2020/10/mobile-app-1.png );">
                        <div class="hitam-trans d-flex justify-content-center align-items-center"
                            style="background-color: rgb(0, 0, 0);">
                            <h2 class="teks-overlay" style="color: white;"> <i class="fa fa-globe" aria-hidden="true"></i>
                            </h2>
                        </div>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Mobile App Development</h5>
                        <p class="card-text">Lorem Ipsum doset amet kontomet anumet ajuajkan </p>
                    </div>
                </div>
            </div>

        </div>


        <div class="row justify-content-md-center p-5" style="background-color: #f5f4f4;">
            <h1 class="text-center mb-5"><span style="color: #E55401">OUR </span><span style="color: #398DA5">PROCESS</span>
            </h1>
            <div class="col-md-10 text-center mb-3 justify-content-center" data-aos="fade-up" data-aos-delay="300"
                data-aos-duration="1000">
                <img src="{{ asset('assets/image/homepage/process.png') }}" width="100%">
            </div>
        </div>

        <div class="row justify-content-md-center p-5">
            <div class="col-md-10 ">
                <div class="row">
                    <div class="col-md-6 " data-aos="fade-up" data-aos-delay="500" data-aos-duration="2000">
                        <img src="{{ asset('assets/image/homepage/desktop.png') }}" width="100%">
                    </div>
                    <div class="col-md-6 " data-aos="fade-left" data-aos-delay="700" data-aos-duration="2000">
                        <h1 class="mb-3"><span style="color: #398DA5">What make us </span><span
                                style="color:#E55401;text-decoration: underline;">different</span><span
                                style="color: #398DA5"> ?</span>
                        </h1>
                        <p class="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                            Ipsum has been
                            the industry's standard dummy text ever since the 1500s
                        </p>
                        <ul>
                            <li>Lorem Ipsum is simply dummy text</li>
                            <li>Lorem Ipsum is simply dummy text</li>
                            <li>Lorem Ipsum is simply dummy text</li>
                            <li>Lorem Ipsum is simply dummy text</li>
                            <li>Lorem Ipsum is simply dummy text</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-md-center p-5 " style="background-color: #F69C00;">
            <h1 style="color: white" class="text-center mb-5">START YOUR NEW PROJECT</h1>
            <div class="col-md-10 text-center mb-3 justify-content-center">
                <form class="row g-3">
                    <div class="col-md-3">
                        <input type="text" class="form-control" id="inputCity" placeholder="Your name">
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" id="inputCity" placeholder="Your name">
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" id="inputCity" placeholder="Your name">
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-primary" style="width: 100%">Submit</button>
                    </div>
                </form>
            </div>
        </div>


        <div class="row justify-content-md-center text-center ">
            <p class="mt-5" style="color: black">TAKE A LOOK</p>
            <h1 style="color: black">OUR <span style="color:#398DA5;"> PORTFOLIO</h1>
            <div class="col-md-11 mb-5">
                <div class="row sliderrr" id="prestasi" data-aos="fade-up" data-aos-delay="300">
                    @foreach ($portofolio as $portofolio)
                        <div class="col-md-3 text-center mt-4">
                            <a relid="{{ $portofolio->gambar_asli }}" class="details"
                                style="color:black;list-style:none;cursor: zoom-in;">

                                <div class="card">
                                    <div class="box-img"
                                        style="background-repeat: no-repeat;background-position: center center;background-size: cover;padding-bottom: 56%;min-width: 100%;background-image: url( {{ asset('assets/image/homepage/portofolio/' . $portofolio->gambar_asli) }} );position: relative;transition: all 0.5s ease 0s;"
                                        }}">
                                    </div>
                                    <div class="card-body" style="padding: 0.5rem!important;">
                                        <p class="card-title" style="font-weight:bold;margin:0;">{{ $portofolio->judul }}
                                        </p>
                                    </div>
                                </div>

                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center text-center ">
            <p class="mt-5" style="color: black">TAKE A LOOK</p>
            <h1 style="color: black">OUR <span style="color:#398DA5;"> CLIENT</h1>
            <div class="col-md-11 mb-5">
                <div class="row sliderrr" id="prestasi" data-aos="fade-up" data-aos-delay="300">
                    @foreach ($portofolio as $portofolio)
                        <div class="col-md-3 text-center mt-4">
                            <a relid="{{ $portofolio->gambar_asli }}" class="details"
                                style="color:black;list-style:none;cursor: zoom-in;">

                                <div class="card">
                                    <div class="box-img"
                                        style="background-repeat: no-repeat;background-position: center center;background-size: cover;padding-bottom: 56%;min-width: 100%;background-image: url( {{ asset('assets/image/homepage/portofolio/' . $portofolio->gambar_asli) }} );position: relative;transition: all 0.5s ease 0s;"
                                        }}">
                                    </div>
                                    <div class="card-body" style="padding: 0.5rem!important;">
                                        <p class="card-title" style="font-weight:bold;margin:0;">{{ $portofolio->judul }}
                                        </p>
                                    </div>
                                </div>

                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>

    @push('script')



        <script type="text/javascript">
            $(function() {
                AOS.init();
            });

            $('.sliderrr').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
            });

        </script>
    @endpush

@endsection
