<style type="text/css">
    .float {
        position: fixed;
        width: 50px;
        height: 50px;
        bottom: 25px;
        right: 25px;
        background-color: #25d366;
        color: #FFF;
        border-radius: 50px;
        text-align: center;
        font-size: 30px;
        box-shadow: 2px 2px 3px #999;
        z-index: 100;
    }

    .float:hover {
        color: #077c33;
    }

    .my-float {
        /* margin-top:16px;*/
    }

</style>

<a href="https://api.whatsapp.com/send?phone=6285739957841" class="float" target="_blank" alt="let's chat us">
    <i class="fab fa-whatsapp my-float" style="position: absolute;top: 10px;right: 12px;"></i>
</a>

<footer class="footer-wrap-layout1 section-shape1 p-5" style="list-style:none;background: rgb(80, 78, 78);color:white">
    <div class="container">
        <div class="row justify-content-center footer-bawah ">
            <div class="col-md-2">
                <img src="{{ asset('assets/image/logo.png') }}" width="100%">
            </div>
            <div class="col-md-3">
                <h3>Company</h3>
                <div class="footer-bottom-menu">
                    <ul style="list-style:none;">
                        <li><a href="#">Sitemap</a></li>
                        <li><a href="#">Terms of Service</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <h3>Company</h3>
                <div class="footer-bottom-menu">
                    <ul style="list-style:none;">
                        <li><a href="#">Sitemap</a></li>
                        <li><a href="#">Terms of Service</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <h3>Contact US</h3>
                <div class="footer-bottom-menu">
                    <ul style="list-style:none;padding:0">
                        <li><a href="#"><i class="fas fa-phone"></i> +62 8515 7340 225</a></li>
                        <li><a href="#"><i class="fal fa-envelope"></i> info@geoncompany.com</a></li>
                    </ul>
                    <h3 class="mt-4">Follow US</h3>
                    <ul style="list-style:none;padding:0">
                        <li style="display:inline;margin-right:15px"><a href="#"><i class="fas fa-phone"></i> +62 8510
                                225</a>
                        </li>
                        <li style="display:inline"><a href="#"><i class="fal fa-envelope"></i> iany.com</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="copyright text-center p-3">Copyright 2021 GEON Company. All Rights Reserved.
</div>
